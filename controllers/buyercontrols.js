const buyermodel = require("../models/buyermodel");


module.exports = {


    addbuyer: function(req,res) {
        const buyer = new buyermodel ( {
            name:req.body.name,
            lastName:req.body.lastName,
            email:req.body.email,
            password:req.body.password,
            cin:req.body.cin,
            numTel:req.body.numTel,
            role:req.body.role,
        });

        buyer.save(function(err) {

            if (err) {
                res.json({
                    msg:'error occured'})
            }
            else {
                res.json({
                    msg:'buyer has been added'})
            }
        })
    },
    listbuyer: function(req,res){
        buyermodel.find({},function(err,buyers){
            if (err){
                res.json({msg:"error occured while retrieving buyers list"})
            }
            else {
                res.json(buyers)
            }
        })
    },
    findbyID: function(req,res){
        buyermodel.find({_id:req.params.id},function(err,buyer){
            if (err){
                res.json({msg:"error occured white retirieving buyer by ID"})
            }
            else {
                res.json(buyer)
            }
        })
    },
    deletebuyer:function(req,res) {
        buyermodel.deleteOne({_id:req.params.id},function(err,buyer){
            if (err) {
                res.json({msg:"error occured while removing buyer by ID"})
            }
            else {
                res.json(buyer)
            }
        })
    },
    updatebuyer:function(req,res) {
        buyermodel.updateOne({ _id:req.params.id},
            {$set:req.body},
            {name:req.body.name,
            lastName:req.body.lastName,
            email:req.body.email,
            password:req.body.password,
            cin:req.body.cin,
            numTel:req.body.numTel},
            function(err,user){
                

                    if (err) {
                        res.json({msg:"error occured while updating buyer"})
                    }
                    else {
                        res.json(user)
                    }
                
                    
        })
        

    }
    
} 