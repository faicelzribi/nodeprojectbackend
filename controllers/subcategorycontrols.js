const subcategorymodel = require('../models/subcategorymodel');

module.exports = {
    addsubcategory:function(req,res){
       const subcategory= new subcategorymodel({
           name:req.body.name,
           products:req.body.products,
       });
       subcategory.save(function(err){
           if(err){
               res.json({msg:'error while creating a subcategory!'})
           }
           else{
               res.json({msg:"subcategory created successfully!"})
           }
       })
    },
    listsubcategory:function(req,res){
        subcategorymodel.find({},function(err,subcategory){
            if (err){
                res.json({msg:"error retrieving subcategory list"})
            }
            else{
                res.json(subcategory)
            }
        })
    },
    deletesubcategory:function(req,res){
        subcategorymodel.deleteOne({_id:req.params.id},function(err){
            if (err){
                res.json({msg:'error deleting subcategory'})
            }
            else{
                res.json({msg:'subcategory deleted successfully:'})
            }
        })
    },
    findbyID:function(req,res){
        subcategorymodel.findOne({_id:req.params.id}).populate({path:'product'}).exec(function(err,category){
            if(err){
                res.json({msg:'error retrieving subategory by ID'})
            }
            else
                res.json(category)
                
        })
    },
    updatesubcategory:function(req,res){
        subcategorymodel.updateOne(
            {_id:req.params.id},
              {$set:req.body}, 
               {name:req.body.name},
               function(err,category){
                if (err){
                    res.json({msg:'updating subcategory failed'})
                }
                else{
                    res.json(category + "subcategory updated successfully!")
                }
            })
    },







}