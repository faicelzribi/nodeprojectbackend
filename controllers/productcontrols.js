const productmodel = require('../models/productmodel');
var fs = require('fs');
const multer=require('multer');
const upload = multer({dest:__dirname +'/productassests/images/'});


module.exports ={
    addproduct:function(req,res){


        var file = __dirname +'/productassests/images/'+req.file.originalname;

        fs.readFile(req.file.path, function (err, data) {

            fs.writeFile(file, data, function (err) {
                
                const product=new productmodel(
                    {name:req.body.name,
                    price:req.body.price,
                    image: req.file.originalname,
                    description:req.body.description,}
                )
                product.save(function(err){
                    if (err){
                        res.json({msg:"adding product failed"})
                    }
                    else {
                        res.json({msg:"product added successfully!"})
                    }
                })   
                
            })
        })
        




        

    },
    listproduct:function(req,res){
        productmodel.find({},function(err,product){
            if (err){
                res.json({msg:"retrieving product list failed"})
            }
            else {
                res.json(product)
            }
        })
    },
    findbyIDproduct:function(req,res){
        productmodel.findOne({_id:req.params.id},function(err,product){
            if (err){
                res.json({msg:"retirieving product by ID failed"})
            }
            else {
                res.json(product)
            }
        })
    },
    updateproduct:function(req,res){

        
        
        productmodel.updateOne({_id:req.params.id},
            {$set:{name:req.body.name,
            price:req.body.price,
            image:req.file.originalname,
            description:req.body.description}},
            function(err,product){
                if (err) {
                    res.json({msg:"updating product failed"})
                }
                else {
                    res.json(product)
                }           
            }    
        )

    },
    deleteproduct:function(req,res){
        productmodel.deleteOne({_id:req.params.id},function(err){
            if (err){
                res.json({msg:'deleting product failed'})
            }
            else{
                res.json({msg:'product deleted successfully!'})
            }
        })
    },
    viewproductimage: function(req,res){
        res.sendFile(__dirname + '/productassests/images/' + req.params.image)
    },







}