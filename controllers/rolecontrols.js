const rolemodel = require('../models/rolemodel');

module.exports = {
    addrole:function(req,res){
        const role = new rolemodel({
            name:req.body.name,
        })
        role.save(function(err){
        if (err){
            res.json({msg:"adding new role failed"})
        }
        else{
            res.json({msg:'role added successfully'})
        }
    })},
    showrole:function(req,res){
        rolemodel.find({},function(err,roles){
            if (err){
                res.json({msg:'retrieving roles failed'})
            }
            else {
                res.json(roles)
            }
        })
    },
}