const sellermodel = require('../models/sellermodel');

var fs = require('fs');
const multer = require('multer');
const upload = multer({dest:__dirname +'/uploads/images'});



module.exports = {

    addseller: function (req, res) {


        var file = __dirname +'/uploads/images/'+req.file.originalname;

        fs.readFile(req.file.path, function (err, data) {

            fs.writeFile(file, data, function (err) {
                
                    if (err) {
                        res.json({msg :'error uploading seller image'})

                    }
                    else {

                        const seller = new sellermodel({

                          
                            image: req.file.originalname,
                            name:req.body.name,
                            lastName:req.body.lastName,
                            email:req.body.email,
                            password:req.body.password,
                            numTel:req.body.numTel,
                            role:req.body.role,

                        });
                        seller.save(function (err) {

                            if (err) {
                                res.json({msg: 'error occured whilte creating a seller'})

                            }
                            else {

                                res.json({ msg: 'seller added successfully '})
                            }


                        })
                    }
                
            })
        })
    },
    listsellers: function(req,res){
        sellermodel.find({},function(err,sellers){
            if (err){
                res.json({msg:"error occured while retrieving sellers list"})
            }
            else {
                res.json(sellers)
            }
        })
    },
    findbyID: function(req,res){
        sellermodel.find({_id:req.params.id},function(err,seller){
            if (err){
                res.json({msg:"error occured while retrieving seller by ID"})
            }
            else {
                res.json(seller)
            }
        })
    },
    deleteseller:function(req,res) {
        sellermodel.deleteOne({_id:req.params.id},function(err,seller){
            if (err) {
                res.json({msg:"error occured while removing user ${}",id})
            }
            else {
                res.json(seller)
            }
        })
    },
    sellerupdate: function (req, res) {
        
        sellermodel.updateOne({_id: req.params.id},
            {$set:{name:req.body.name,
            lastName:req.body.lastName,
            email:req.body.email,
            password:req.body.password,
            image:req.file.originalname,numTel:req.body.numTel}},
    
          function (err,data) {
    
            if (err) {
    
              res.json({ msg: 'product not found'+err })
            }
            else {
              res.json({ msg: 'product updated successfully'+data} )
            }
    
        })
},

  
       
    viewimage:function(req,res){
        res.sendFile(__dirname + '/uploads/images/' + req.params.image)
    },
}
