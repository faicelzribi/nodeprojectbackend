const usermodel = require("../models/usermodel"); //importing usermodel
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");



module.exports = {
    
    adduser:function(req,res){       // req: data sent from client to server    res: data sent from  server to cleint
            const user = new usermodel({
                name:req.body.name,
                lastName:req.body.lastName,
                email:req.body.email,
                password:req.body.password,
                role:req.body.role,
                numTel:req.body.numTel,
            });
            user.save(function(err)
            {
                if (err)
                //using API rest that's why we write res.json
                {
                    res.json({
                        msg:"error occured"+err})}
                else
                {
                    res.json({
                        msg:"user has been added"})}
                           }    )
    },
    authenticateuser: function(req, res,next) {
        usermodel.findOne({email:req.body.email}).populate('role').exec(function(err, userInfo){
           if (err) {
            next(err);
           } 
           else {
               if(bcrypt.compareSync(req.body.password, userInfo.password)) {
                   const token = jwt.sign({id: userInfo._id}, req.app.get('secretKey'), { expiresIn: '1h' });
                   res.json({msg: "user found", data:{user: userInfo, token:token}});
                }
                else{
                    res.json({msg: "Invalid email/password", data:null});
                }
            }
        });
    },
    listuser:function(req,res){
        usermodel.find({},function(err,users){
            if (err){
            res.json({msg:"erro occured while listing users"})
            }
            else {
                res.json(users)
            }
        })
    },
    findbyID:function(req,res) {
        usermodel.find({_id: req.params.id},function(err,user){
            if (err){
                res.json({msg:"error occured while retrieving user by ID"})
            }
            else {
                res.json(user)
            }
        })
    },
    deleteuser:function(req,res) {
        usermodel.deleteOne({_id:req.params.id},function(err,user){
            if (err) {
                res.json({msg:"error occured while removing user by id"})
            }
            else {
                res.json(user)
            }
        })
    },
    updateuser:function(req,res) {
        usermodel.updateOne({ _id:req.params.id}
            ,{$set:req.body},
            {name:req.body.name,
            lastName:req.body.lastName,
            email:req.body.email,
            password:req.body.password,},
            
            function(err,user){
                    if (err) {
                        res.json({msg:"error occured while updating user"})
                    }
                    else {
                        res.json(user)
                    }           
        })
        

    }

   
    

    





}