const categorymodel = require('../models/categorymodel');

module.exports = {

    addcategory:function(req,res){     
        const category = new categorymodel({
            name:req.body.name,
            subcategory:req.body.subcategory,
        });
        category.save(function(err)
        {
            if (err)
           
            {
                res.json({
                    msg:"adding category failed"})}
            else
            {
                res.json({
                    msg:"category has been added succefully!"})}
                       }    )
},
    listcategories:function(req,res){
        categorymodel.find({},function(err,categories){
            if(err){
                res.json({msg:"error occured while retrieving category list"})
            }
            else{
                res.json(categories)
            }
            
        })
    },
    findbyID:function(req,res){
        categorymodel.find({_id:req.params.id}).populate({
            path:'subcategory',
            populate:{ path:'product'}}).exec(function(err,category){
            if (err){
                res.json({
                    msg:"error occured while finding by ID the relevant category"})
            }
            else 
            res.json(category)
            
        })
    },
    deletecategory:function(req,res){
        categorymodel.deleteOne({_id:req.params.id},function(err,category){
            if (err){
                res.json({
                    msg:"error occured while deleting a category"
                })
            }
            else{
                res.json(category)
            }
        })
    },
    updatecategory:function(req,res) {
        categorymodel.updateOne({ _id:req.params.id},
            {$set:req.body},
            {name:req.body.name},
            
            function(err,category){
                    if (err) {
                        res.json({msg:"error occured while updating user!"})
                    }
                    else {
                        res.json(category +" Note: category updated successfully!")
                        
                    }           
        })
    },
    
}

