
const express = require('express'); //importing express module

const app = express(); //app includes functions and methodes provided by express module

var jwt = require('jsonwebtoken');
app.set('secretKey', 'nodeRestApi'); // jwt secret token


const bodyParser = require("body-parser");

const userrouter = require("./routers/userrouter");

const buyerrouter = require('./routers/buyerrouter');

const sellerrouter= require('./routers/sellerrouter');

const categoryrouter= require('./routers/categoryrouter');

const subcategoryrouter= require('./routers/subcategoryrouter');

const productrouter=require('./routers/productrouter');
const rolerouter= require('./routers/rolerouter');

const db = require('./models/db');




app.use(bodyParser.json());// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));// parse application/json

app.use("/user",userrouter); //use: is middleware function that executes after server send the request before delivering response to the client side
app.use("/buyer",buyerrouter);
app.use("/seller",sellerrouter);
app.use("/category",categoryrouter);
app.use('/subcategory',subcategoryrouter);
app.use('/product',productrouter);
app.use('/role',rolerouter);

//communicating with client side

app.listen(8080,function(){ //80 port      //function() is call back function generate errors if there is some and defines message shown on console when server is running
   
  
    console.log('server is running') 
})
