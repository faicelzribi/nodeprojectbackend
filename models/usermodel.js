const mongoose= require('mongoose');   //importing mongoose
const bcrypt= require('bcryptjs');

const Schema=mongoose.Schema;  //importing Schema to create collections of data base (like table db sql)

const baseoptions= {
    discriminatorKey:"usertype", //our discriminatorkey could be anything
    collection:"users" //the name our colletion in date base
}

const userSchema=mongoose.model('user',new mongoose.Schema({ //creating mongoose Schema
    //creating attributes of user model in mongo DB
    name:{                                                
        type:String,
        required:true,
        trim:true,
    },
    lastName:{
        type:String,
        required:true,
        trim:true,
    },
    email:{
        type:String,
        required:true,
        trim:true,
    },
    password:{
        type:String,
        required:true,
        trim:true,
    },
    numTel:{
        type:Number,
        required:true,
    },
    role:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'role',
    },

    


})
.pre('save',function(){
    this.password = bcrypt.hashSync(this.password,10);
    //stocks hashs in data base ('mongoDb')
})

)



module.exports=userSchema;