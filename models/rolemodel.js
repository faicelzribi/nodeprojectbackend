const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const roleSchema = mongoose.model('role',new mongoose.Schema({
    name:{
        type:String,
        required:true,
    }
}))
module.exports = roleSchema;