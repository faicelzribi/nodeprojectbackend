const mongoose = require("mongoose");
const user = require('../models/usermodel')

const Schema = mongoose.Schema;

const sellerSchema= user.discriminator('seller',new mongoose.Schema({
    image:{
        
       type:String,
        required:true,
    },
    
}))

module.exports = sellerSchema;
