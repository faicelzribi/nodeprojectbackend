const mongoose=require('mongoose');
const user = require('../models/usermodel')

const Schema=mongoose.Schema;

const buyerSchema=user.discriminator('buyer',new mongoose.Schema({
    cin:{
        type:Number,
        required:true,
        trim:true,
    },
    numTel:{
        type:Number,
        required:true,
        trim:true,
    },
}

))
module.exports=buyerSchema;