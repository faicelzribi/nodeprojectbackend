const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const productSchema=mongoose.model('product',new mongoose.Schema({
    name:{
        type:String,
        required:true,
    },
    price:{
        type:Number,
        required:true,
        trim:true,
    },
    image:{
        type:String,
        required:true,
    },
    description:{
        type:String,
        required:true,
    }
}));

module.exports = productSchema;