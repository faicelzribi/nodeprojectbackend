const mongoose = require('mongoose');
const Schema = mongoose.Schema;

subcategorySchema=mongoose.model('subcategory',new mongoose.Schema({
    name:{
        type:String,
        required:true,
        trim:true,
    },
    product:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'product',
    }

})
)

module.exports = subcategorySchema;