const usercontrols = require("../controllers/usercontrols")

const router = require("express").Router();

router.post('/adduser',usercontrols.adduser);
router.post('/authenticate', usercontrols.authenticateuser);
router.get('/listuser',usercontrols.listuser);
router.get('/findbyID/:id',usercontrols.findbyID);
router.delete('/delete/:id',usercontrols.deleteuser);
router.put('/update/:id',usercontrols.updateuser);


module.exports = router;