const categorycontrols = require('../controllers/categroycontrols');

const router = require("express").Router();

router.post('/addcategory',categorycontrols.addcategory);
router.get('/listcategories',categorycontrols.listcategories);
router.get('/findbyID/:id',categorycontrols.findbyID);
router.delete('/delete/:id',categorycontrols.deletecategory);
router.put('/update/:id',categorycontrols.updatecategory);

module.exports = router;