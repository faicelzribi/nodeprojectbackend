const rolecontrols = require('../controllers/rolecontrols');

const router = require('express').Router();

router.post('/addrole',rolecontrols.addrole);
router.get('/showrole',rolecontrols.showrole);

module.exports=router;