const buyercontrols = require("../controllers/buyercontrols");

const router = require ("express").Router();

router.post('/addbuyer',buyercontrols.addbuyer);
router.get('/listbuyer',buyercontrols.listbuyer);
router.get('/findbyID/:id',buyercontrols.findbyID);
router.delete('/delete/:id',buyercontrols.deletebuyer);
router.put('/update/:id',buyercontrols.updatebuyer);

module.exports = router;