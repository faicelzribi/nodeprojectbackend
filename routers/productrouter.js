const productcontrols= require('../controllers/productcontrols');
const router=require('express').Router();

const multer = require('multer');
const upload = multer({dest:__dirname + '/productassests/images/'});

router.post('/addproduct',upload.single('image'),productcontrols.addproduct);
router.get('/listproduct',productcontrols.listproduct);
router.get('/findbyIDproduct/:id',productcontrols.findbyIDproduct);
router.put('/updateproduct/:id',upload.single('image'),productcontrols.updateproduct);
router.delete('/deleteproduct/:id',productcontrols.deleteproduct);
router.get('/viewproductimage/:image',productcontrols.viewproductimage);



module.exports=router;