const subcategorycontrols = require('../controllers/subcategorycontrols');

const router = require('express').Router();

router.post('/addsubcategory',subcategorycontrols.addsubcategory);
router.get('/listsubcategory',subcategorycontrols.listsubcategory);
router.delete('/deletesubcategory/:id',subcategorycontrols.deletesubcategory);
router.get('/findbyIDsubcategory/:id',subcategorycontrols.findbyID);
router.put('/updatesubcategory/:id',subcategorycontrols.updatesubcategory);





module.exports=router;