const sellercontrols = require("../controllers/sellercontrols");

const router = require("express").Router();

const multer = require('multer');
const upload = multer({dest:__dirname + '/uploads/images/'});

 
 router.get('/listsellers',sellercontrols.listsellers);
 router.get('/findbyID/:id',sellercontrols.findbyID);
 router.delete('/delete/:id',sellercontrols.deleteseller);
 router.put('/update/:id',upload.single('image'),sellercontrols.sellerupdate);
 router.post('/addseller',upload.single('image'),sellercontrols.addseller);
 router.get('/viewimage/:image',sellercontrols.viewimage);
 

module.exports = router;